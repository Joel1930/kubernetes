[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md/
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8/
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/



# Kubernetes Demo Umgebung

- [Installation](md/installation/README.md)
    * [OS Installation](md/installation/centos.md)
    * [Clone Worker Node](md/installation/clone.md)
    * [Kubernetes Setup](md/installation/clusterinit.md)
- [Dashboard](md/dashboard/README.md)
    * [Zertifikate](md/dashboard/certificate.md)
    * [Dashboard](md/dashboard/dashboard.md)
- [Volumes](md/volumes/README.md)
    * [NFS Server](md/volumes/nfsserver.md)
    * [Persistent Volume](md/volumes/persistent.md)
    * [Persistent Volume Claim](md/volumes/claim.md)
- [Deployment](md/deployment/README.md)
    * [MySQL](md/deployment/mysql.md)
    * [Jenkins](md/deployment/jenkins.md)
- [Secrets](md/secrets/REDME.md)
    * [Secrets](md/secrets/einleitung.md)
- [k8s Administration](md/administration/README.md)
    * [kubectl](md/administration/kubectl.md)

