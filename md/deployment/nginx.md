[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md/
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8/
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# nginx - Webserver

**nginx-Webserver** deployen.

Beispieldateien speichern unter `$HOME/deployment/mysql`.

```
# mkdir -p /$HOME/deployment/nginx
# cd /$HOME/deployment/nginx
```

## Persistente Daten

Die Applikation verwendet folgenden persitenten Storage

`/usr/share/nginx/html/web-app` --> `pvc-nfs-platin` --> `storageClassName: platin` --> `nfsserver:/mnt/nfs_shares/platin`

Entsprechend wird der Web-Content von nginx unter `nfsserver:/mnt/nfs_shares/platin` gespeichert.

### Demo Datei anlegen

Auf dem NFS Server werden pro NFS-Share ein `index.html` File mit entsprechendem Inhalt angelegt.

```
nfsserver> # echo "Hello, NFS Storage platin" > /mnt/nfs_shares/platin/index.html
nfsserver> # echo "Hello, NFS Storage gold" > /mnt/nfs_shares/gold/index.html
nfsserver> # echo "Hello, NFS Storage silver" > /mnt/nfs_shares/silver/index.html
```

## Deployment

Pro NFS Share ein Deployment und zugehöriger Service mit entsprechendem Label anlegen.

### Deployment *silver*
```
# cat <<EOF>> /$HOME/deployment/nginx/nginx-silver.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-nfs-silver-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-silver
  template:
    metadata:
      labels:
        app: nginx-silver
    spec:
      volumes:
      - name: webcontent
        persistentVolumeClaim:
          claimName: pvc-nfs-silver
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        volumeMounts:
        - name: webcontent
          mountPath: "/usr/share/nginx/html/"
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-nfs-silver-service
spec:
  selector:
    app: nginx-silver
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
EOF
```

### Deployment *gold*

```
# cat <<EOF>> /$HOME/deployment/nginx/nginx-gold.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-nfs-gold-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-gold
  template:
    metadata:
      labels:
        app: nginx-gold
    spec:
      volumes:
      - name: webcontent
        persistentVolumeClaim:
          claimName: pvc-nfs-gold
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        volumeMounts:
        - name: webcontent
          mountPath: "/usr/share/nginx/html/"
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-nfs-gold-service
spec:
  selector:
    app: nginx-gold
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
EOF
```

### Deployment *platin*

```
# cat <<EOF>> /$HOME/deployment/nginx/nginx-platin.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-nfs-platin-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-platin
  template:
    metadata:
      labels:
        app: nginx-platin
    spec:
      volumes:
      - name: webcontent
        persistentVolumeClaim:
          claimName: pvc-nfs-platin
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        volumeMounts:
        - name: webcontent
          mountPath: "/usr/share/nginx/html/"
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-nfs-platin-service
spec:
  selector:
    app: nginx-platin
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
EOF
```

## Deploy nginx

```

# kubectl create -f nginx-silver.yaml
deployment.apps/nginx-nfs-silver-deployment created
service/nginx-nfs-silver-service created

# kubectl create -f nginx-gold.yaml
deployment.apps/nginx-nfs-gold-deployment created
service/nginx-nfs-gold-service created


# kubectl create -f nginx-platin.yaml
deployment.apps/nginx-nfs-platin-deployment created
service/nginx-nfs-platin-service created
```

## Testen

*nginx* Service ermitteln
```
# # kubectl get service
NAME                       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
kubernetes                 ClusterIP   10.96.0.1        <none>        443/TCP   2d7h
nginx-nfs-gold-service     ClusterIP   10.109.75.147    <none>        80/TCP    2m16s
nginx-nfs-platin-service   ClusterIP   10.111.66.193    <none>        80/TCP    114s
nginx-nfs-silver-service   ClusterIP   10.102.132.195   <none>        80/TCP    2m32s
```

mit `curl` testen

```
# curl http://10.109.75.147
Hello, NFS Storage gold

# curl http://10.111.66.193
Hello, NFS Storage platin

# curl http://10.102.132.195
Hello, NFS Storage silver
```

